import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.MatOfFloat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.TermCriteria;
import org.opencv.ml.CvSVM;
import org.opencv.ml.CvSVMParams;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Mat;

public class Main {
	
	public static void main(String[] args) {
		//load opencv library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		/************* Acquire and parse CSV file to matrix *************/
		String filename;
		
		//opencv uses an outdated version of jre -> unable to use lists
		//preallocate 2D int array for storing csv data
		//ctr counts the number of samples in the csv
		int ctr = 0;
		float[][] data = new float[100][10000];
		
		System.out.print("Enter csv filename: ");
		Scanner reader = new Scanner(System.in);
		filename = reader.next();
		reader.close();
		
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		
		//parse csv file
		try{
			br = new BufferedReader(new FileReader(filename));
			System.out.println("Parsing Data");
			while((line = br.readLine()) != null){
				String[] text = line.split(splitBy);
				for(int x = 0; x<10000; x++){
					data[ctr][x] = Float.parseFloat(text[x]);
					
				}
				ctr++;
			}
			System.out.println("Parsing Finished");
		}catch (Exception e){
			e.printStackTrace();
		}
		
		/************* SVM Training with OpenCV *************/
		//create and initialize training matrix
		Mat trainingMat = new Mat(ctr,10000,CvType.CV_32FC1);
		
		for(int x=0; x<ctr; x++){
			trainingMat.put(x, 0, data[x]);
		}
		
		//create and initialize label matrix
		//first half of csv file should contain the 1.0f class and remaining the -1.0f class
		float[] labels = new float[ctr];
		Mat labelsMat = new Mat(ctr,1,CvType.CV_32FC1);
		
		for(int x=0; x<ctr; x++){
			if(x<ctr/2){
				labels[x] = 1.0f;
			}else{
				labels[x] = -1.0f;
			}
		}
		
		labelsMat.put(0, 0, labels);
		
	    //initialize SVM parameters
	    CvSVMParams params = new CvSVMParams();
	    TermCriteria crit = new TermCriteria(TermCriteria.MAX_ITER,100,1e-6);
	    
	    params.set_C(CvSVM.C_SVC);
	    params.set_kernel_type(CvSVM.LINEAR);
	    params.set_term_crit(crit);
	    
	    //initialize and train svm
	    CvSVM svm = new CvSVM();
	    svm.train(trainingMat, labelsMat, new Mat(), new Mat(),params);

	    //save model output to xml
	    svm.save("model.xml");

	    System.out.println("Done saving SVM model");
	    
	}
}
